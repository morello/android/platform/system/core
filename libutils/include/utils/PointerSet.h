/*
 * Copyright (C) 2022 Arm Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _UTILS_POINTER_SET_H
#define _UTILS_POINTER_SET_H

#include <shared_mutex>
#include <unordered_set>

namespace android {

/*
 * Thread-safe unordered set of pointers.
 */
template<typename T>
class PointerSet {
public:
    /* Creates an empty pointer set */
    PointerSet() {}

    /* For thread-safety reasons, the object can't be moved or copied */
    PointerSet(const PointerSet&) =delete;
    PointerSet(PointerSet&&) =delete;
    PointerSet& operator=(const PointerSet&) =delete;
    PointerSet& operator=(PointerSet&&) =delete;

    /* Adds a pointer to the set if it is not there yet.
     * Comparison is happening by the address component, so if there is another
     * pointer with the same address value in the set already, nothing changes. */
    void add(T* ptr) {
      std::unique_lock<std::shared_timed_mutex> lock(mLock);

      mSet.insert(ptr);
    }

    /* Remove a pointer from the set.
     * Comparison is happening by the address component only. */
    void remove(T* ptr) {
        std::unique_lock<std::shared_timed_mutex> lock(mLock);

        mSet.erase(ptr);
    }

    /* Find pointer in the set by the address component.
     * If there is no pointer with the given address component in the set, then
     * nullptr is returned. */
    T* lookup(ptraddr_t addr) {
        std::shared_lock<std::shared_timed_mutex> lock(mLock);

        uintptr_t null_derived_cap = addr;

        auto it = mSet.find(reinterpret_cast<T*>(null_derived_cap));

        if (it == mSet.end()) {
          return nullptr;
        }

        return *it;
    }

    /* Find pointer in the set - matching by the address component.
     * If there is no pointer with the matching address component in the set, then
     * nullptr is returned. */
    T* lookup(T* ptr) {
        return lookup(static_cast<ptraddr_t>(reinterpret_cast<uintptr_t>(ptr)));
    }

private:
    std::shared_timed_mutex mLock;
    std::unordered_set<T*> mSet;
};

} // namespace android

#endif /* _UTILS_POINTER_SET_H */
