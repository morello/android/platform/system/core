/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cutils/misc.h>

#include <string.h>

#include <cutils/properties.h>

bool
is_hardware_morello_fvp() {
    char prop_hardware[PROP_VALUE_MAX];
    return (property_get("ro.boot.hardware", prop_hardware, "")
            && strncmp(prop_hardware, "morello_fvp", PROP_VALUE_MAX) == 0);
}

bool
is_profile_morello_nano() {
    char prop_system_name[PROP_VALUE_MAX];
    return (property_get("ro.product.system.name", prop_system_name, "")
            && strstr(prop_system_name, "morello_nano") == prop_system_name);
}
